<?php

namespace Pixelcircus\LatestTweets;

use League\Container\ServiceProvider\AbstractServiceProvider;

class LatestTweetsProvider extends AbstractServiceProvider
{

    protected $provides = [
        'Pixelcircus\LatestTweets\LatestTweets',
        'latestTweets.auth',
    ];

    public function register()
    {
		$config = $this->getContainer()->get('config')['latestTweets'];
        $this->getContainer()->add('latestTweets.auth', 'Abraham\TwitterOAuth\TwitterOAuth')
                ->withArguments([$config['consumerKey'], $config['consumerSecret'], $config['accessToken'], $config['accessTokenSecret']]);
        $this->getContainer()->add('Pixelcircus\LatestTweets\LatestTweets')->withArguments(['latestTweets.auth', $config['screenName']]);
    }
}
