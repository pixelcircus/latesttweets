<?php

namespace Pixelcircus\LatestTweets;

class LatestTweets
{

    protected $auth;
    protected $screenName;
    protected $logger;

    public function __construct(\Abraham\TwitterOAuth\TwitterOAuth $auth, $screenName, \Psr\Log\LoggerInterface $logger = null)
    {
        $this->auth = $auth;
        $this->screenName = $screenName;
        $this->logger = $logger;
    }

    public function __invoke($limit = 1)
    {
        try {
            $tweets = $this->auth->get('statuses/user_timeline', ['screen_name' => $this->screenName, 'count' => $limit]);

            if (isset($tweets->errors)) {
                $errorMsg = 'Cannot retrieve latest tweets. Error message from Twitter api: ' . $tweets->errors[0]->code . ' / ' . $tweets->errors[0]->message;
                if (!is_null($this->logger)) {
                    $this->logger->critical($errorMsg);
                } else {
                    trigger_error($errorMsg, E_USER_WARNING);
                }
                return false;
            }

            $items = [];

            foreach ($tweets as $tweet) {
                $items[] = new Tweet($tweet);
            }
        } catch (\Exception $e) {
            $items = [];
        }

        return $items;
    }

}
