<?php

namespace Pixelcircus\LatestTweets;

class Tweet
{
    
    protected $data;
    
    /**
     * 
     * @param stdClass $data Response from twitter API
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function screenName()
    {
        return '@'.$this->data->user->screen_name;
    }

    public function userLink()
    {
        return 'https://twitter.com/'.$this->data->user->screen_name;
    }
    
    public function followLink()
    {
        return 'https://twitter.com/intent/user?screen_name='.$this->data->user->screen_name;
    }

    public function message()
    {
        return $this->addLinks($this->data->text);
    }

    public function dateShort()
    {
        return (new \DateTime($this->data->created_at))->format('j M');
    }
    
    public function date()
    {
        return (new \DateTime($this->data->created_at))->format('Y-m-d');
    }

    protected function addLinks($text)
    {
        $text = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $text);
        $text = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $text);
        $text = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $text);
        $text = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $text);

        return $text;
    }

}
